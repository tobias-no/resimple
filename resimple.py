#!/usr/bin/env python

import os
import sys

import numpy as np
import SimpleITK as sitk

import logging as log

loglevel = log.DEBUG
log.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=loglevel)

class ResampleSimple(object):
    def __init__(self, source_file):
        log.info("Reading image from {}...".format(source_file))
        self.original_image = sitk.ReadImage(source_file)
        self.original_spacing = self.original_image.GetSpacing()
        if loglevel == log.DEBUG:
            log.debug("Basic info of original image:")
            self.print_image_info(self.original_image)

    def print_image_info(self, img):
        print("Spacing: {}".format(img.GetSpacing()))
        print("Size: {}".format(img.GetSize()))
        print("Direction: {}".format(img.GetDirection()))
        print("Origin: {}".format(img.GetOrigin()))
        #print("PixelIDValue: {}".format(img.GetPixelIDValue()))

    def get_new_size(self, size, spacing, target_spacing):
        ## equation: original_spacing * original_size = target_spacing * target_size
        target_size = np.array(spacing) * np.array(size) / np.array(target_spacing)

        return target_size.astype(int)

    def resample_whole_image(self, target_spacing=[1.,1.,1.]):
        try:
            assert len(target_spacing) == self.original_image.GetDimension()
        except AssertionError as e:
            log.critical("wrong dimension of input file -> check input file: {}".format(e))
            sys.exit(-1)

        resample = sitk.ResampleImageFilter()
        target_size = self.get_new_size(self.original_image.GetSize(),
                                        self.original_image.GetSpacing(),
                                        target_spacing)

        res_filter = sitk.ResampleImageFilter()
        res_filter.SetOutputSpacing(target_spacing)
        res_filter.SetSize([int(target_size[0]),
                            int(target_size[1]),
                            int(target_size[2])])
        res_filter.SetOutputDirection(self.original_image.GetDirection())
        res_filter.SetOutputOrigin(self.original_image.GetOrigin())
        res_filter.SetTransform(sitk.Transform())
        #res_filter.SetInterpolator(sitk.sitkLanczosWindowedSinc) #eewig
        #res_filter.SetInterpolator(sitk.sitkLinear)
        res_filter.SetInterpolator(sitk.sitkNearestNeighbor)
        target_image = res_filter.Execute(self.original_image)

        if loglevel == log.DEBUG:
            log.debug("Basic info of target image:")
            self.print_image_info(target_image)

        return target_image

    def transform_mask_single_voxel(self, target_spacing=[1.,1.,1.]):
        try:
            assert len(target_spacing) == self.original_image.GetDimension()
        except AssertionError as e:
            log.critical("wrong dimension of input file -> check input file: {}".format(e))
            sys.exit(-1)

        #beware of c convention with [z, y, x] and itk convention with [x, y, z]
        a = sitk.GetArrayFromImage(self.original_image)
        try:
            assert len(a[a>0]) == 1 #only one voxel should be marked
        except AssertionError as e:
            log.critical("more than one voxel marked -> check input file: {}".format(e))
            sys.exit(-1)

        original_coordinates = np.argwhere(a==1)[0][::-1] #[x,y,z]

        log.debug("original mask marked: {}".format(original_coordinates))
        new_coordinates = self.get_new_size(original_coordinates,
                                            self.original_image.GetSpacing(),
                                            target_spacing)

        new_size = self.get_new_size(self.original_image.GetSize(),
                                     self.original_image.GetSpacing(),
                                     target_spacing)
        new_mask = sitk.Image(int(new_size[0]),
                              int(new_size[1]),
                              int(new_size[2]),
                              sitk.sitkUInt8)
        new_mask.SetOrigin(self.original_image.GetOrigin())
        new_mask.SetSpacing(target_spacing)
        new_mask.SetDirection(self.original_image.GetDirection())

        new_mask.SetPixel(int(new_coordinates[0]),
                          int(new_coordinates[1]),
                          int(new_coordinates[2]),
                          1)

        if loglevel == log.DEBUG:
            log.debug("Basic info of new mask:")
            self.print_image_info(new_mask)
            log.debug("new mask marked: {}".format(new_coordinates))
            log.debug("new physical coordinates: {}".format(
                       new_mask.TransformIndexToPhysicalPoint([int(new_coordinates[0]),
                                                               int(new_coordinates[1]),
                                                               int(new_coordinates[2])])
                                                              ))

            log.debug("original physical coordinates: {}".format(
                      self.original_image.TransformIndexToPhysicalPoint([int(original_coordinates[0]),
                                                                         int(original_coordinates[1]),
                                                                         int(original_coordinates[2])])
                                                                        ))

        return new_mask

    def dilation(self, mask, a): #a is area of max circle in cm2
        log.info("performing dilation on mask with a max area of {}cm2".format(a))
        spacing = mask.GetSpacing()
        try:
            assert spacing[0] == spacing[1] == spacing[2]
        except AssertionError as e:
            log.critical("spacing in different directions noch matching {}".format(e))
            sys.exit(-1)

        a_mm = a * 100 #conversion to mm2
        r = np.sqrt(a_mm/np.pi)
        r_voxel_continous = r / spacing[0]
        r_voxel = int(np.rint(r_voxel_continous))
        final_mask = sitk.BinaryDilate(mask, [r_voxel] * 3)

        if loglevel == log.DEBUG:
            a = sitk.GetArrayFromImage(final_mask)
            final_volume = a[a==1].size * spacing[0]**3 / 1000

            log.debug("theoretical volume: {}".format(4/3 * np.pi * (r/10)**3))
            log.debug("final volume: {}".format(final_volume))

        return final_mask

    def save_image(self, img, target_file):
        log.info("Saving transformed image to... {}".format(target_file))
        sitk.WriteImage(img, target_file)


if __name__ == "__main__":
    original_imgs_folder = "E:/tono/data/prepare_balls/original_imgs"
    resampled_imgs_folder = "E:/tono/data/prepare_balls/resampled_imgs"
    for root, dirs, files in os.walk(original_imgs_folder):
        for f in files:
            print('*'*50)
            rs = ResampleSimple(os.path.join(root,f))
            img = rs.resample_whole_image([rs.original_spacing[0], rs.original_spacing[0], rs.original_spacing[0]])
            rs.save_image(img, os.path.join(resampled_imgs_folder, f.split('.')[0] + "_resampled.nrrd"))

    log.info("Images resampling finished")

    center_masks_folder = "E:/tono/data/prepare_balls/centers"
    final_balls_folder = "E:/tono/data/prepare_balls/final_balls"
    for root, dirs, files in os.walk(center_masks_folder):
        for f in files:
            print('*'*50)
            rs = ResampleSimple(os.path.join(root,f))
            t_mask = rs.transform_mask_single_voxel([rs.original_spacing[0], rs.original_spacing[0], rs.original_spacing[0]])
            final_mask = rs.dilation(t_mask, 4)
            rs.save_image(final_mask, os.path.join(final_balls_folder, f.split('.')[0] + "_finalball.nii.gz"))

    log.info("Ball masks finished")
